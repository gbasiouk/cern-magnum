# cern-magnum

This is the umbrella chart behind all the add-ons available for CERN Kubernetes
clusters.

Check the default `values.yaml` for the multiple configuration options
available.

## Chart Repositories

Charts are expected to exist under registry.cern.ch, all dependencies must
refer to our local repository.

There are two main repositories currently in use:
* [stable](https://registry.cern.ch/harbor/projects/1295/helm-charts): upstream dependencies (old stable repo). A [replication rule](https://registry.cern.ch/harbor/replications) `artifact-hub` takes care of keeping them in sync with upstream versions
* [cern](): charts maintained locally, mostly coming from our [cern charts](https://gitlab.cern.ch/helm/charts/cern) repo
